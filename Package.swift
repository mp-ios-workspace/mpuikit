// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MPUIKit",
    platforms: [
        .iOS(.v11),
        .tvOS(.v11)
    ],
    products: [
        .library(
            name: "MPUIKit",
            targets: ["MPUIKit"])
    ],
    dependencies: [
        .package(
            url: "https://github.com/SDWebImage/SDWebImage.git",
            .exact("5.13.4")
        ),
        .package(
            url: "https://github.com/SnapKit/SnapKit",
            .exact("5.6.0")
        )
    ],
    targets: [
        .target(
            name: "MPUIKit",
            dependencies: ["SDWebImage", "SnapKit"]),
    ]
)
