
import UIKit

public protocol MPModule where Controller: UIViewController {
    associatedtype Controller
    var viewController: Controller { get }
}
