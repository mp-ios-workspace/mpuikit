
public protocol MPClosable {
    func close(completion: MPClosure?)
    func close()
    func closeToRoot(completion: MPClosure?)
    func closeToRoot()
}

public extension MPClosable where Self: MPRouterProtocol {
    
    func close(completion: MPClosure?) {
        close(completion: completion)
    }
    
    func close() {
        close(completion: nil)
    }
    
    func closeToRoot(completion: MPClosure?) {
        closeToRoot(completion: completion)
    }
    
    func closeToRoot() {
        closeToRoot(completion: nil)
    }
    
}
