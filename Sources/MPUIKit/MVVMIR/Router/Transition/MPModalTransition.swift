
import UIKit

open class MPModalTransition: MPTransition {
    
    open weak var viewController: UIViewController?
    
    private let presentationStyle: UIModalPresentationStyle
    private let transitionStyle: UIModalTransitionStyle
    
    public init(presentationStyle: UIModalPresentationStyle = .overFullScreen, transitionStyle: UIModalTransitionStyle = .coverVertical) {
        self.presentationStyle = presentationStyle
        self.transitionStyle = transitionStyle
    }
    
    open func open(_ viewController: UIViewController, completion: MPClosure? = nil) {
        viewController.modalPresentationStyle = presentationStyle
        viewController.modalTransitionStyle = transitionStyle
        self.viewController?.present(viewController, animated: true, completion: completion)
    }
    
    open func close(completion: MPClosure?) {
        self.viewController?.dismiss(animated: true, completion: completion)
    }
    
    open func closeToRoot(completion: MPClosure?) {
        self.viewController?.dismiss(animated: true, completion: completion)
    }
    
}
