
import UIKit

open class MPPushTransition: MPTransition {
    
    open weak var viewController: UIViewController?
    
    public init() {}
    
    open func open(_ viewController: UIViewController, completion: MPClosure? = nil) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
    
    open func close(completion: MPClosure?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.viewController?.navigationController?.popViewController(animated: true)
        CATransaction.commit()
    }
    
    open func closeToRoot(completion: MPClosure?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.viewController?.navigationController?.popToRootViewController(animated: true)
        CATransaction.commit()
    }
    
}
