
import UIKit

public protocol MPTransition: AnyObject {
    var viewController: UIViewController? { get set }
    func open(_ viewController: UIViewController, completion: MPClosure?)
    func close(completion: MPClosure?)
    func closeToRoot(completion: MPClosure?)
}

public extension MPTransition where Self == MPModalTransition {
    
    static func modal(
        presentationStyle: UIModalPresentationStyle = .overFullScreen,
        transitionStyle: UIModalTransitionStyle = .coverVertical
    ) -> MPModalTransition {
        .init(presentationStyle: presentationStyle, transitionStyle: transitionStyle)
    }
    
    @available(iOS 13.0, *)
    static var modalAutomatic: MPModalTransition {
        .init(presentationStyle: .automatic, transitionStyle: .coverVertical)
    }
    
}

public extension MPTransition where Self == MPPushTransition {
    
    static var push: MPPushTransition {
        .init()
    }
    
}
