
import UIKit

public protocol MPRouterProtocol {
    associatedtype ViewController: UIViewController
    var viewController: ViewController? { get }
    func open(_ viewController: UIViewController, transition: MPTransition)
    func open(_ viewController: UIViewController, transition: MPTransition, completion: MPClosure?)
    func close(completion: MPClosure?)
    func closeToRoot(completion: MPClosure?)
}

open class MPRouter<Controller>: MPRouterProtocol where Controller: UIViewController {
    
    open weak var viewController: Controller?
    open var openTransition: MPTransition?
    
    public init() {}
    
    open func open(_ viewController: UIViewController, transition: MPTransition) {
        DispatchQueue.main.async { [weak self] in
            self?.open(viewController, transition: transition, completion: nil)
        }
    }
    
    open func open(_ viewController: UIViewController, transition: MPTransition, completion: MPClosure? = nil) {
        transition.viewController = self.viewController
        DispatchQueue.main.async {
            transition.open(viewController, completion: completion)
        }
    }
    
    open func close(completion: MPClosure? = nil) {
        DispatchQueue.main.async { [weak self] in
            self?.openTransition?.close(completion: completion)
        }
    }
    
    open func closeToRoot(completion: MPClosure? = nil) {
        DispatchQueue.main.async { [weak self] in
            self?.openTransition?.closeToRoot(completion: completion)
        }
    }
    
}
