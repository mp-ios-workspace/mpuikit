
open class MPInteractor {

    open var didError: MPDataClosure<Error?>?
    open var startLoading: MPClosure?
    open var stopLoading: MPClosure?
    
    public init() {}
    
}
