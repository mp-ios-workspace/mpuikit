
import UIKit

open class MPViewController<ViewModel, Router, Interactor, ErrorHandler>: UIViewController, MPViewProtocol where Interactor: MPInteractor, Router: MPRouter<UIViewController>, ErrorHandler: MPErrorHandler, ViewModel: MPViewModel<Interactor, Router> {
    
    open var viewModel: ViewModel
    public let errorHandler: ErrorHandler
    
    public init(viewModel: ViewModel, errorHandler: ErrorHandler) {
        self.viewModel = viewModel
        self.errorHandler = errorHandler
        super.init(nibName: nil, bundle: nil)
        
        viewModel.didError = { [weak self] in
            self?.errorHandler.handle(error: $0)
        }
        viewModel.startLoading = { [weak self] in
            DispatchQueue.main.async {
                self?.startLoading()
            }
        }
        viewModel.stopLoading = { [weak self] in
            DispatchQueue.main.async {
                self?.stopLoading()
            }
        }
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        updateComponents()
        
        viewModel.viewDidLoad()
    }
    
    open func setupComponents() {
        
    }
    
    open func updateComponents() {
        
    }
    
    open func startLoading() {
        
    }
    
    open func stopLoading() {
        
    }
    
}
