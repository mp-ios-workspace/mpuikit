
import UIKit

open class MPViewModel<Interactor: MPInteractor, Router: MPRouter<UIViewController>> {

    public let interactor: Interactor
    public let router: Router
    
    open var startLoading: MPClosure?
    open var stopLoading: MPClosure?
    open var didError: MPDataClosure<Error?>?
    
    public init(interactor: Interactor, router: Router) {
        self.router = router
        self.interactor = interactor
        interactor.startLoading = { [weak self] in
            self?.startLoading?()
        }
        interactor.stopLoading = { [weak self] in
            self?.stopLoading?()
        }
        interactor.didError = { [weak self] in
            self?.didError?($0)
        }
    }
    
    open func viewDidLoad() {
        
    }
    
}
