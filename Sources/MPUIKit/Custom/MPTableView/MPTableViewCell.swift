
import UIKit

import UIKit

open class MPTableViewCell: UITableViewCell {
    
    // MARK: - Props
    open var _viewModel: Any? {
        didSet { updateComponents() }
    }
    
    // MARK: - Init
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupComponents()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupComponents()
    }

    // MARK: - Methods
    open override func awakeFromNib() {
        super.awakeFromNib()

        setupComponents()
    }
    
    open override func setSelected(_ selected: Bool, animated: Bool) {
        return
    }
    
    open override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        return
    }

    // MARK: - MPViewProtocol
    open func setupComponents() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    open func updateComponents() {}
    
}

