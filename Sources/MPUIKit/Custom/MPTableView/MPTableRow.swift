
import UIKit

open class MPTableRow {
    
    // MARK: - Props
    open var cellIdentifier: String? {
        nil
    }

    open var cellHeight: CGFloat {
        UITableView.automaticDimension
    }
    
    open var cellEstimatedHeight: CGFloat {
        30
    }
    
    public var didTap: MPClosure?
    public var isFirstSectionRow = false
    public var isLastSectionRow = false
    public var isFirstTableRow = false
    public var isLastTableRow = false

    // MARK: - Init
    public init(didTap: MPClosure? = nil) {
        self.didTap = didTap
    }
    
    // MARK: - Methods
    open func createLeadingSwipeActionsConfiguration(for cell: UITableViewCell) -> UISwipeActionsConfiguration? {
        .init(actions: [])
    }
    
    open func createTrailingSwipeActionsConfiguration(for cell: UITableViewCell) -> UISwipeActionsConfiguration? {
        .init(actions: [])
    }
    
    open func willBeginEditing(for cell: UITableViewCell) { }
    
    open func didEndEditing(for cell: UITableViewCell) { }
    
}
