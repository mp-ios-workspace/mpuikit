
import UIKit

open class MPTableDataSourceAdapter: NSObject, UITableViewDataSource {
    
    // MARK: - Props
    open weak var tableView: MPTableView? {
        didSet {
            tableView?.dataSource = self
        }
    }
    
    open var sections: [MPTableSection] {
        get {
            tableView?.sections ?? []
        }
        set {
            tableView?.sections = newValue
        }
    }
    
    // MARK: - UITableViewDataSource
    open func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }

    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard sections.indices.contains(section) else { return .zero }
        
        return sections[section].rows.count
    }

    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let row = sections.getRow(at: indexPath),
            let cellIdentifier = row.cellIdentifier,
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MPTableViewCell
        else { return .init() }
        
        row.isFirstTableRow = indexPath == .zero
        row.isLastTableRow = indexPath.section == sections.count - 1 && indexPath.row == (sections.last?.rowsCount ?? 0) - 1
        
        row.isFirstSectionRow = indexPath.row == 0
        row.isLastSectionRow = indexPath.row == sections[indexPath.section].rowsCount - 1
        
        cell._viewModel = row
        
        if let tableView = self.tableView {
            tableView.cellsOutput?.cellForRow(tableView, indexPath: indexPath, cell: cell)
        }

        return cell
    }
    
}
