
import UIKit

open class MPTableHeaderFooterView<ViewModel: MPTableHeaderFooter>: UITableViewHeaderFooterView {
    
    // MARK: - Model
    open var viewModel: ViewModel? {
        didSet {
            self.updateComponents()
        }
    }
    
    // MARK: - Init
    override public init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)

        self.updateComponents()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.setupComponents()
    }

    // MARK: - Methods
    open override func awakeFromNib() {
        super.awakeFromNib()

        self.setupComponents()
    }

    // MARK: - DPViewProtocol
    open func setupComponents() {}
    
    open func updateComponents() {}

}
