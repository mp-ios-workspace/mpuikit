
import UIKit

open class MPTableHeaderFooter {

    open var viewIdentifier: String? {
        nil
    }

    open var viewHeight: CGFloat {
        UITableView.automaticDimension
    }

    open var viewEstimatedHeight: CGFloat {
        0
    }
    
    public init() {}
    
}
