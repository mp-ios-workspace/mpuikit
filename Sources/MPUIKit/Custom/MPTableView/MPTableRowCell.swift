

open class MPTableRowCell<ViewModel: MPTableRow>: MPTableViewCell {
    
    // MARK: - Props
    open var viewModel: ViewModel? {
        get { _viewModel as? ViewModel }
        set { _viewModel = newValue }
    }
    
}
