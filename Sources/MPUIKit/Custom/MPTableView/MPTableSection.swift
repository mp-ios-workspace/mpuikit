
import UIKit

open class MPTableSection {

    // MARK: - Props
    open var rows: [MPTableRow]
    open var header: MPTableHeaderFooter?
    open var footer: MPTableHeaderFooter?

    // MARK: - Init
    public init(
        rows: [MPTableRow],
        header: MPTableHeaderFooter? = nil,
        footer: MPTableHeaderFooter? = nil
    ) {
        self.rows = rows
        self.header = header
        self.footer = footer
    }
    
}

public extension MPTableSection {
    
    var rowsCount: Int {
        rows.count
    }
    
    func getRow(at index: Int) -> MPTableRow? {
        guard self.rows.indices.contains(index) else { return nil }
        
        return self.rows[index]
    }
    
}

// MARK: - Array + DPTableSection
public extension Array where Element == MPTableSection {
    
    var rowsCount: Int {
        self.reduce(0, { $0 + $1.rows.count })
    }
    
    var rowsIsEmpty: Bool {
        self.rowsCount == 0
    }
    
    var headersIsEmpty: Bool {
        self.filter({ $0.header != nil }).isEmpty
    }
    
    var footersIsEmpty: Bool {
        self.filter({ $0.footer != nil }).isEmpty
    }
    
    func getRow(at indexPath: IndexPath) -> MPTableRow? {
        guard self.indices.contains(indexPath.section) else { return nil }
        
        return self[indexPath.section].getRow(at: indexPath.row)
    }
    
}
