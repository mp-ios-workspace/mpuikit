
import UIKit

open class MPTableDelegateAdapter: NSObject, UITableViewDelegate {
    
    // MARK: - Props
    open weak var tableView: MPTableView? {
        didSet {
            tableView?.delegate = self
        }
    }
    
    open var sections: [MPTableSection] {
        get {
            tableView?.sections ?? []
        }
        set {
            tableView?.sections = newValue
        }
    }
    
    open internal(set) var lastContentOffset: CGPoint?
    
    // MARK: - Methods
    open func calculateRowsCountOrLess(at indexPath: IndexPath) -> Int {
        sections
            .prefix(indexPath.section + 1)
            .enumerated()
            .reduce(0, { sum, item in
                let section = item.element
                let sectionIndex = item.offset

                let rowsPrefix = sectionIndex == indexPath.section ?
                    indexPath.row + 1 :
                    section.rows.count

                return sum + section.rows.prefix(rowsPrefix).count
            })
    }
    
    open var indexPathOfFirstRow: IndexPath? {
        guard !sections.isEmpty, sections.first?.rows.isEmpty == false else { return nil }

        return .init(row: .zero, section: .zero)
    }

    open var indexPathOfLastRow: IndexPath? {
        guard !sections.isEmpty, sections.first?.rows.isEmpty == false else { return nil }
        let row = (sections.last?.rows.endIndex ?? 1) - 1
        let section = sections.endIndex - 1

        return .init(row: row, section: section)
    }
    
    // MARK: - UITableViewDelegate + Row
    open func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableView = self.tableView else { return }
        
        tableView.cellsOutput?.willDisplayRow(tableView, indexPath: indexPath, cell: cell)

        let offsetToTop = calculateRowsCountOrLess(at: indexPath)
        tableView.dataOutput?.scrollToPosition(tableView, position: .top, rowsOffset: offsetToTop)
        
        if offsetToTop == 0 {
            tableView.dataOutput?.topAchived(tableView)
        }
        
        let offsetToBottom = sections.rowsCount - offsetToTop
        tableView.dataOutput?.scrollToPosition(tableView, position: .bottom, rowsOffset: offsetToBottom)
        
        if offsetToBottom == 0 {
            tableView.dataOutput?.bottomAchived(tableView)
        }
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard
            let tableView = self.tableView,
            let cell = tableView.cellForRow(at: indexPath),
            let model = sections.getRow(at: indexPath)
        else { return }

        model.didTap?()
        tableView.dataOutput?.selectRow(tableView, indexPath: indexPath, cell: cell, row: model)
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let model = sections.getRow(at: indexPath) else { return UITableView.automaticDimension }

        return model.cellHeight
    }
    
    open func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let model = sections.getRow(at: indexPath) else { return UITableView.automaticDimension }

        return model.cellEstimatedHeight
    }
    
    // MARK: - UITableViewDelegate + Scroll
    open func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset

        if lastContentOffset == nil {
            lastContentOffset = offset
        }

        guard
            let lastOffset = lastContentOffset,
            let tableView = tableView
        else { return }

        let scrollToPosition: UITableView.ScrollPosition = lastOffset.y > offset.y ? .top : .bottom
        let bottomOffset = tableView.calculateBottomOffset()
        let isDragging = scrollView.isDragging

        tableView.scrollOutput?.didScroll(tableView, to: scrollToPosition, isDragging: isDragging)

        guard isDragging else { return }

        switch scrollToPosition {
        case .top:
            let pointForCell = CGPoint(x: offset.x, y: offset.y < 0 ? 0 : offset.y)

            if let indexPathForCell = tableView.indexPathForRow(at: pointForCell) {
                let offsetToTop = calculateRowsCountOrLess(at: indexPathForCell)
                tableView.dataOutput?.scrollToPosition(tableView, position: .top, rowsOffset: offsetToTop)
            }
        case .bottom:
            let pointForCell = CGPoint(x: offset.x, y: offset.y + tableView.frame.height)

            if let indexPathForCell = tableView.indexPathForRow(at: pointForCell) {
                let offsetToBottom = sections.rowsCount - calculateRowsCountOrLess(at: indexPathForCell)
                tableView.dataOutput?.scrollToPosition(tableView, position: .bottom, rowsOffset: offsetToBottom)
            }
        default:
            break
        }

        let topAchived = offset.y <= -scrollView.contentInset.top
        let bottomAchived = offset.y >= (bottomOffset.y < 0 ? 0 : bottomOffset.y)

        tableView.scrollOutput?.scrollPositionAchived(tableView, position: .top, isAchived: topAchived)
        tableView.scrollOutput?.scrollPositionAchived(tableView, position: .bottom, isAchived: bottomAchived)

        lastContentOffset = offset
    }
    
    // MARK: - UITableViewDelegate + Header In Section
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard
            let model = sections[section].header,
            let viewIdentifier = model.viewIdentifier,
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: viewIdentifier) as? MPTableHeaderFooterView
        else {
            return nil
        }

        view.viewModel = model
        return view
    }

    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let model = sections[section].header else { return .zero }

        return model.viewHeight
    }

    open func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        guard let model = sections[section].header else { return .zero }

        return model.viewEstimatedHeight
    }

    // MARK: - UITableViewDelegate + Footer In Section
    open func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard
            let model = sections[section].footer,
            let viewIdentifier = model.viewIdentifier,
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: viewIdentifier) as? MPTableHeaderFooterView
        else {
            return nil
        }

        view.viewModel = model
        return view
    }

    open func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let model = sections[section].footer else { return .zero }

        return model.viewEstimatedHeight
    }

    open func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        guard let model = sections[section].footer else { return .zero }

        return model.viewEstimatedHeight
    }

    // MARK: - UITableViewDelegate + Swipe
    open func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard
            let model = sections.getRow(at: indexPath),
            let cell = tableView.cellForRow(at: indexPath)
        else {
            return .init(actions: [])
        }

        return model.createLeadingSwipeActionsConfiguration(for: cell)
    }

    open func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard
            let model = sections.getRow(at: indexPath),
            let cell = tableView.cellForRow(at: indexPath)
        else {
            return .init(actions: [])
        }

        return model.createTrailingSwipeActionsConfiguration(for: cell)
    }

    // MARK: - UITableViewDelegate + Edit
    open func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        guard
            let model = sections.getRow(at: indexPath),
            let cell = tableView.cellForRow(at: indexPath)
        else { return }

        return model.willBeginEditing(for: cell)
    }

    open func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        guard
            let indexPath = indexPath,
            let model = sections.getRow(at: indexPath),
            let cell = tableView.cellForRow(at: indexPath)
        else { return }

        return model.didEndEditing(for: cell)
    }
    
}
