
import UIKit

// MARK: - Data Output
public protocol MPTableDataOutput: AnyObject {
    func beginRefreshing(_ tableView: MPTableView)
    func endRefreshing(_ tableView: MPTableView)
    func selectRow(_ tableView: MPTableView, indexPath: IndexPath, cell: UITableViewCell, row: MPTableRow)
    func scrollToPosition(_ tableView: MPTableView, position: UITableView.ScrollPosition, rowsOffset: Int)
    func topAchived(_ tableView: MPTableView)
    func bottomAchived(_ tableView: MPTableView)
}

public extension MPTableDataOutput {
    func beginRefreshing(_ tableView: MPTableView) {}
    func endRefreshing(_ tableView: MPTableView) {}
    func selectRow(_ tableView: MPTableView, indexPath: IndexPath, cell: UITableViewCell, row: MPTableRow) {}
    func scrollToPosition(_ tableView: MPTableView, position: UITableView.ScrollPosition, rowsOffset: Int) {}
    func topAchived(_ tableView: MPTableView) {}
    func bottomAchived(_ tableView: MPTableView) {}
}

// MARK: - Cells Output
public protocol MPTableCellsOutput: AnyObject {
    func cellForRow(_ tableView: MPTableView, indexPath: IndexPath, cell: UITableViewCell)
    func willDisplayRow(_ tableView: MPTableView, indexPath: IndexPath, cell: UITableViewCell)
}

public extension MPTableCellsOutput {
    func cellForRow(_ tableView: MPTableView, indexPath: IndexPath, cell: UITableViewCell) {}
    func willDisplayRow(_ tableView: MPTableView, indexPath: IndexPath, cell: UITableViewCell) {}
}

// MARK: - Scroll Output
public protocol MPTableScrollOutput: AnyObject {
    func didScroll(_ tableView: MPTableView, to position: UITableView.ScrollPosition, isDragging: Bool)
    func scrollPositionAchived(_ tableView: MPTableView, position: UITableView.ScrollPosition, isAchived: Bool)
}

public extension MPTableScrollOutput {
    func didScroll(_ tableView: MPTableView, to position: UITableView.ScrollPosition, isDragging: Bool) {}
    func scrollPositionAchived(_ tableView: MPTableView, position: UITableView.ScrollPosition, isAchived: Bool) {}
}

// MARK: - View
open class MPTableView: UITableView {
    
    // MARK: - Props
    open override var refreshControl: UIRefreshControl? {
        didSet {
            didSetRefreshControl()
        }
    }
    
    open override var tableHeaderView: UIView? {
        didSet {
            didSetRefreshControl()
        }
    }
    
    open weak var dataOutput: MPTableDataOutput?
    
    open weak var cellsOutput: MPTableCellsOutput?
    
    open weak var scrollOutput: MPTableScrollOutput?
    
    open var dataSourceAdapter: MPTableDataSourceAdapter? {
        didSet {
            didSetDataSourceAdapter()
        }
    }
    
    open var delegateAdapter: MPTableDelegateAdapter? {
        didSet {
            didSetDelegateAdapter()
        }
    }
    
    open var sections: [MPTableSection] = [] {
        didSet {
            updatePlaceholderViewAutoHidden()
        }
    }
    
    open var placeholderView: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
            backgroundView = placeholderView
        }
    }
    
    open var placeholderViewAutoHiddenEnabled: Bool = true
    
    // MARK: - Init
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        setupComponents()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupComponents()
    }
    
    // MARK: - Methods
    open func didSetDataSourceAdapter() {
        dataSourceAdapter?.tableView = self
    }
    
    open func didSetDelegateAdapter() {
        delegateAdapter?.tableView = self
    }
    
    open func didSetRefreshControl() {
        guard let refreshControl = refreshControl else { return }

        refreshControl.addTarget(self, action: #selector(refreshControlValueChanged(_:)), for: .valueChanged)
        bringSubviewToFront(refreshControl)
    }
    
    @objc
    open func refreshControlValueChanged(_ refreshControl: UIRefreshControl) {
        dataOutput?.beginRefreshing(self)
    }
    
    open func beginRefreshing() {
        refreshControl?.sendActions(for: .valueChanged)
        reloadData()
    }

    open func endRefreshing() {
        refreshControl?.endRefreshing()
        dataOutput?.endRefreshing(self)
    }
    
    open func updatePlaceholderViewAutoHidden() {
        guard placeholderViewAutoHiddenEnabled else { return }
        
        var isEmpty: Bool {
            sections.isEmpty && sections.rowsIsEmpty && sections.headersIsEmpty && sections.footersIsEmpty
        }
        
        placeholderView?.isHidden = !isEmpty
    }
    
    open func reloadData(with sections: [MPTableSection]) {
        self.sections = sections
        reloadData()
    }
    
    // MARK: - MPViewProtocol
    open func setupComponents() {
        delegateAdapter = .init()
        dataSourceAdapter = .init()
    }
    
}
