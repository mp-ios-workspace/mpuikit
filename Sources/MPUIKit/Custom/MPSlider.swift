
import UIKit

open class MPSlider: UISlider {
    
    open var didChange: MPDataClosure<Float>? {
        didSet {
            addTarget(self, action: #selector(valueDidChanged), for: .valueChanged)
        }
    }
    
    @objc private func valueDidChanged() {
        didChange?(value)
    }
    
}
