
import UIKit

public struct MPToast {
    
    public struct Configuration {
        public var cornerRadius: CGFloat
        public var backgroundColor: UIColor?
        public var textColor: UIColor?
        public var textAlignment: NSTextAlignment?
        public var attributedText: NSAttributedString?
        public var font: UIFont?
        public var textInsets: UIEdgeInsets
        public var duration: TimeInterval
        fileprivate let uuid = UUID().uuidString
        
        public init(
            cornerRadius: CGFloat = 0,
            backgroundColor: UIColor? = nil,
            textColor: UIColor? = nil,
            textAlignment: NSTextAlignment? = nil,
            attributedText: NSAttributedString? = nil,
            font: UIFont? = nil,
            textInsets: UIEdgeInsets = .init(top: 6, left: 4, bottom: 4, right: 6),
            duration: TimeInterval = 2
        ) {
            self.cornerRadius = cornerRadius
            self.backgroundColor = backgroundColor
            self.textColor = textColor
            self.textAlignment = textAlignment
            self.attributedText = attributedText
            self.font = font
            self.textInsets = textInsets
            self.duration = duration
        }
    }
    
    public static var isQueueEnabled = false
    private static var toastQueue: [(text: String?, configuration: Configuration)] = []
    private static var currentToastView: UIView?
    
    private init() {}
    
    public static func show(text: String?, configuration: Configuration = .init()) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        isQueueEnabled ? toastQueue.append((text, configuration)) : currentToastView?.removeFromSuperview()
        
        DispatchQueue.main.async {
            let toastView = UIView()
            toastView.setup(
                .alpha(0),
                .cornerRadius(configuration.cornerRadius),
                .backgroundColor(configuration.backgroundColor ?? UIColor.black.withAlphaComponent(0.7))
            )
            
            let label = UILabel()
            label.setup(
                .text(text),
                .numberOfLines(0),
                .textAlignment(configuration.textAlignment ?? .center),
                .font(configuration.font ?? .systemFont(ofSize: 14, weight: .regular)),
                .textColor(configuration.textColor ?? .white)
            )
            if let attributedText = configuration.attributedText {
                label.attributedText = attributedText
            }
            
            label.addToSuperview(toastView, withConstraints: [
                .edgesToSuperview(configuration.textInsets)
            ])
            
            let safeArea = window.safeAreaLayoutGuide
            toastView.addToSuperview(window, withConstraints: [
                .centerXEqualTo(safeArea.centerXAnchor),
                .leadingGreaterThanOrEqualTo(safeArea.leadingAnchor, offset: 16),
                .trailingGreaterThanOrEqualTo(safeArea.trailingAnchor, constant: 16),
                .bottomEqualTo(safeArea.bottomAnchor, constant: 10),
                .topGreaterThanOrEqualTo(safeArea.topAnchor, constant: 10)
            ])
            
            UIView.animate(withDuration: 0.2) {
                toastView.alpha = 1
            }
            
            currentToastView = toastView
            
            DispatchQueue.main.asyncAfter(deadline: .now() + configuration.duration) { [weak toastView] in
                UIView.animate(withDuration: 0.2) {
                    toastView?.alpha = 0
                } completion: { _ in
                    toastView?.removeFromSuperview()
                }
                
                if isQueueEnabled {
                    toastQueue.removeAll(where: { $0.configuration.uuid == configuration.uuid })
                    guard let nextToast = toastQueue.first else {
                        return
                    }
                    show(text: nextToast.text, configuration: nextToast.configuration)
                }
            }
        }
    }
    
}
