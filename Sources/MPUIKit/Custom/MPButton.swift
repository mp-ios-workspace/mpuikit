
import UIKit

open class MPButton: UIButton {
    
    open var didTap: MPClosure? {
        didSet {
            addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        }
    }
    
    @objc private func didTapAction() {
        didTap?()
    }
    
}
