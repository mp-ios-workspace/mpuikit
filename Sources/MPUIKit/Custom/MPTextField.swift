
import UIKit

open class MPTextField: UITextField {
    
    open var editingChanged: MPDataClosure<String?>? {
        didSet {
            addTarget(self, action: #selector(editingDidChanged), for: .editingChanged)
        }
    }
    
    open var editingBegin: MPClosure? {
        didSet {
            addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
        }
    }
    
    open var editingEnd: MPClosure? {
        didSet {
            addTarget(self, action: #selector(editingDidEnd), for: .editingDidEnd)
        }
    }
    
    open var debouncerDuration: TimeInterval?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc private func editingDidBegin() {
        editingBegin?()
    }
    
    @objc private func editingDidEnd() {
        editingEnd?()
    }
    
    @objc private func editingDidChanged() {
        guard let duration = debouncerDuration else {
            editingChanged?(text)
            return
        }
        
        DispatchQueue.main.asyncDeduped(target: self, after: duration) { [weak self] in
            guard self?.isFirstResponder == true else {
                return
            }
            self?.editingChanged?(self?.text)
        }
    }
    
}
