
import UIKit

open class MPModelControl<ViewModel>: MPControl, MPViewProtocol {
    
    open var viewModel: ViewModel? {
        didSet { updateComponents() }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupComponents()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupComponents()
    }
    
    open func setupComponents() {}
    
    open func updateComponents() {}
    
}

open class MPControl: UIControl {

    open var minAlpha: CGFloat = 0.4
    open var didTap: MPClosure? {
        didSet {
            addTarget(
                self,
                action: #selector(controlDidTap),
                for: .touchUpInside
            )
        }
    }
    
    open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer.isKind(of: UITapGestureRecognizer.self) else {
            return super.gestureRecognizerShouldBegin(gestureRecognizer)
        }
        return false
    }

    open func animate(alpha: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = alpha
        }
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        animate(alpha: minAlpha)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        animate(alpha: 1)
        sendActions(for: .touchUpInside)
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        animate(alpha: 1)
    }

    @objc private func controlDidTap() {
        didTap?()
    }

}
