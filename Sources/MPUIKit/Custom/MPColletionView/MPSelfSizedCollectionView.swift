
import UIKit

open class MPSelfSizedColletionView: UICollectionView {
    
    open override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    open override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        let height = collectionViewLayout.collectionViewContentSize.height
        return CGSize(width: UIView.noIntrinsicMetric, height: height)
    }
    
}
