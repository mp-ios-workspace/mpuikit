
import UIKit

open class MPCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Props
    public var _viewModel: Any? {
        didSet { updateComponents() }
    }
    
    // MARK: - Init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupComponents()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupComponents()
    }
    
    // MARK: - Methods
    open override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    // MARK: - MPViewProtocol
    open func setupComponents() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    open func updateComponents() {}
    
}
