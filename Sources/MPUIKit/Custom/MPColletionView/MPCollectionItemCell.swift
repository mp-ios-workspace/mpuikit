
import Foundation

open class MPCollectionItemCell<ViewModel: MPCollectionItem>: MPCollectionViewCell {
    
    // MARK: - Props
    open var viewModel: ViewModel? {
        get { _viewModel as? ViewModel }
        set { _viewModel = newValue }
    }
    
}
