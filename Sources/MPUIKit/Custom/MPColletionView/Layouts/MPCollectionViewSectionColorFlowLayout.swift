
import UIKit

@objc public protocol UICollectionViewSectionColorDelegateFlowLayout: UICollectionViewDelegateFlowLayout {
    @objc optional func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, backgroundColor section: Int) -> UIColor?
}

open class MPCollectionViewSectionColorLayoutAttributes: UICollectionViewLayoutAttributes {
    open var sectionBackgroundColor: UIColor?
}

open class MPCollectionViewSectionColorReusableView: UICollectionReusableView {

    override open func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let att = layoutAttributes as? MPCollectionViewSectionColorLayoutAttributes {
            backgroundColor = att.sectionBackgroundColor
        }
    }
}

open class MPCollectionViewSectionColorFlowLayout: UICollectionViewFlowLayout {

    open var decorationViewAttrs = [MPCollectionViewSectionColorLayoutAttributes]()

    override public init() {
        super.init()
        initialSetup()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        initialSetup()
    }
    
    private func initialSetup() {
        register(MPCollectionViewSectionColorReusableView.self, forDecorationViewOfKind: "UICollectionViewSectionColorReusableView")
    }
    
    override open func prepare() {
        super.prepare()
        
        //Section Number
        guard let collectionView = collectionView else { return }
        let numberOfSections = collectionView.numberOfSections
        guard let delegate = collectionView.delegate as? UICollectionViewSectionColorDelegateFlowLayout
        else {
            return
        }
        decorationViewAttrs.removeAll()
        for section in 0..<numberOfSections {
            let numberOfItems = collectionView.numberOfItems(inSection: section)
            guard numberOfItems > 0,
                  let firstItem = layoutAttributesForItem(at: IndexPath(item: 0, section: section)),
                  let lastItem = layoutAttributesForItem(at: IndexPath(item: numberOfItems - 1, section: section)) else {
                      continue
                   }
            var sectionInset = self.sectionInset
            if let inset = delegate.collectionView?(collectionView, layout: self, insetForSectionAt: section) {
                sectionInset = inset
            }
            var sectionFrame = firstItem.frame.union(lastItem.frame)
            sectionFrame.origin.x = 0
            sectionFrame.origin.y -= sectionInset.top
            if scrollDirection == .horizontal {
                sectionFrame.size.width += sectionInset.left + sectionInset.right
                sectionFrame.size.height = collectionView.frame.height
            } else {
                var collectionViewBottomInset: CGFloat = 0
                if section == collectionView.numberOfSections - 1 {
                    //to fill empty bottom space
                    collectionViewBottomInset = 300
                }
                
                sectionFrame.size.width = collectionView.frame.width
                sectionFrame.size.height += sectionInset.top + sectionInset.bottom + collectionViewBottomInset
            }

            // 2、 Define view properties
            let attr = MPCollectionViewSectionColorLayoutAttributes(forDecorationViewOfKind: "UICollectionViewSectionColorReusableView", with: IndexPath(item: 0, section: section))
            attr.frame = sectionFrame
            attr.zIndex = -1
            attr.sectionBackgroundColor = delegate.collectionView?(collectionView, layout: self, backgroundColor: section)
            decorationViewAttrs.append(attr)
        }
    }
    
    override open func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {

        guard let attributes = super.layoutAttributesForElements(in: rect) else { return nil }
        var allAttributes = [UICollectionViewLayoutAttributes]()
        allAttributes.append(contentsOf: attributes)
        for att in decorationViewAttrs {
            if rect.intersects(att.frame) {
                allAttributes.append(att)
            }
        }
        return allAttributes
    }
}
