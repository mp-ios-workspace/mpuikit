
import UIKit

open class MPTabBarController: UITabBarController {
    
    // MARK: - Props
    open var items: [MPTabBarItem] = []
    
    // MARK: - Methods
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setupComponents()
    }
    
    open var selectedItem: MPTabBarItem? {
        get {
            items.first(where: { $0.tag == self.selectedIndex })
        }
        set {
            guard
                let tag = newValue?.tag,
                items.contains(where: { $0.tag == tag })
            else {
                return
            }
            
            selectedIndex = tag
        }
    }
    
    open func setupComponents() {
        
    }
    
}

