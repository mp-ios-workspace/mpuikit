//
//  File.swift
//  
//
//  Created by Alexander on 04.02.2022.
//

import UIKit

open class MPNavigationController: UINavigationController, UIGestureRecognizerDelegate {
    
    // MARK: - Methods
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    // MARK: - DPViewProtocol
    open func setupComponents() {
        self.interactivePopGestureRecognizer?.delegate = self
    }
    
    // MARK: - UIGestureRecognizerDelegate
    open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count != 1
    }
    
}
