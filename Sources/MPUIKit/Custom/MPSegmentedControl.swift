
import UIKit

public protocol SegmentItem: Equatable {
    var title: String { get }
}

open class MPSegmentedControl<Segment: SegmentItem>: UISegmentedControl {
    
    open var didSelectSegment: MPDataClosure<Segment>? {
        didSet {
            addTarget(
                self,
                action: #selector(didSelectSegmentAction),
                for: .valueChanged
            )
        }
    }
    
    open var items: [Segment] {
        didSet { setupItems() }
    }
    
    open var selectedItem: Segment?
    
    public init(items: [Segment], selectedItem: Segment? = nil) {
        self.items = items
        self.selectedItem = selectedItem ?? items.first
        super.init(frame: .zero)
        setupItems()
    }
    
    required public init?(coder: NSCoder) {
        items = []
        super.init(coder: coder)
        setupItems()
    }
    
    public func setSelected(font: UIFont?) {
        guard let font = font else { return }
        setTitleTextAttributes([.font: font], for: .selected)
    }
    
    public func setNormal(font: UIFont?) {
        guard let font = font else { return }
        setTitleTextAttributes([.font: font], for: .normal)
    }
    
    private func setupItems() {
        removeAllSegments()
        items.enumerated().forEach {
            insertSegment(
                withTitle: $0.element.title,
                at: $0.offset,
                animated: false
            )
        }
        
        let index = items.firstIndex(where: { $0 == selectedItem })
        selectedSegmentIndex = index ?? 0
    }
    
    @objc private func didSelectSegmentAction() {
        didSelectSegment?(items[selectedSegmentIndex])
    }
    
}
