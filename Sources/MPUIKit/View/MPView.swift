
import UIKit

public protocol MPViewProtocol {
    associatedtype ViewModel
    
    var viewModel: ViewModel { get set }
    
    func setupComponents()
    
    func updateComponents()
}

open class MPView<ViewModel>: UIView, MPViewProtocol {
    
    open var viewModel: ViewModel? {
        didSet { updateComponents() }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupComponents()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupComponents()
    }
    
    open func setupComponents() {}
    
    open func updateComponents() {}
    
}
