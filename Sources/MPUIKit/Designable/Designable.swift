
import UIKit

public protocol Designable { }

public extension Designable {
    
    static func style(style: @escaping Style<Self>) -> Style<Self> { return style }
    
    @discardableResult
    func setup(_ styles: [StyleWrapper<Self>]) -> Self {
        styles.forEach({ style in
            switch style {
            case let .wrap(style):
                style(self)
            }
        })
        
        return self
    }
    
    @discardableResult
    func setup(_ styles: StyleWrapper<Self>...) -> Self {
        self.setup(styles)
    }
    
}

extension NSObject: Designable {}
