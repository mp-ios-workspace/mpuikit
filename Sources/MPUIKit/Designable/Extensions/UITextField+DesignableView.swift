
import UIKit

public extension StyleWrapper where Element: UITextField {
    
    static func textAlignment(_ value: NSTextAlignment) -> Self {
        .wrap { textField in
            textField.textAlignment = value
        }
    }
    
    static func keyboardType(_ value: UIKeyboardType) -> Self {
        .wrap { textField in
            textField.keyboardType = value
        }
    }
    
    static func autocapitalizationType(_ value: UITextAutocapitalizationType) -> Self {
        .wrap { textField in
            textField.autocapitalizationType = value
        }
    }
    
    static func textColor(_ value: UIColor) -> Self {
        .wrap { textField in
            textField.textColor = value
        }
    }
    
    static func placeholder(_ text: String?) -> Self {
        .wrap { textField in
            textField.placeholder = text
        }
    }
    
    static func borderStyle(_ value: UITextField.BorderStyle) -> Self {
        .wrap { item in
            item.borderStyle = value
        }
    }
    
    static func font(_ value: UIFont) -> Self {
        .wrap { item in
            item.font = value
        }
    }
    
    static func isSecureTextEntry(_ value: Bool) -> Self {
        .wrap { item in
            item.isSecureTextEntry = value
        }
    }
    
}
