
import UIKit

public extension StyleWrapper where Element: UIScrollView {
    
    static func `default`(contentView: UIView) -> Self {
        .wrap { item in
            item.setup(
                .keyboardDismissMode(.interactive),
                .addContentView(contentView)
            )
        }
    }
    
    static func showsHorizontalScrollIndicator(_ value: Bool) -> Self {
        return .wrap { scrollView in
            scrollView.showsHorizontalScrollIndicator = value
        }
    }
    
    static func showsVerticalScrollIndicator(_ value: Bool) -> Self {
        return .wrap { scrollView in
            scrollView.showsVerticalScrollIndicator = value
        }
    }
    
    static func contentInset(_ value: UIEdgeInsets) -> Self {
        .wrap { scrollView in
            scrollView.contentInset = value
        }
    }
    
    static func bounces(_ value: Bool) -> Self {
        .wrap { item in
            item.bounces = value
        }
    }
    
    static func keyboardDismissMode(_ value: UIScrollView.KeyboardDismissMode) -> Self {
        .wrap { item in
            item.keyboardDismissMode = value
        }
    }
    
    static func addContentView(_ value: UIView) -> Self {
        .wrap { item in
            value.translatesAutoresizingMaskIntoConstraints = false
            item.translatesAutoresizingMaskIntoConstraints = false
            item.addSubview(value)

            NSLayoutConstraint.activate([
                value.topAnchor.constraint(equalTo: item.topAnchor),
                value.bottomAnchor.constraint(equalTo: item.bottomAnchor),
                value.leadingAnchor.constraint(equalTo: item.leadingAnchor),
                value.trailingAnchor.constraint(equalTo: item.trailingAnchor),
                value.widthAnchor.constraint(equalTo: item.widthAnchor)
            ])
        }
    }
    
}
