
import UIKit

public extension StyleWrapper where Element: UITableView {
    
    static var `default`: Self {
        .wrap { item in
            item.rowHeight = UITableView.automaticDimension
            item.backgroundColor = .clear
            item.separatorStyle = .none
            item.delaysContentTouches = false
            item.allowsSelectionDuringEditing = false
            item.keyboardDismissMode = .onDrag
            item.sectionHeaderHeight = UITableView.automaticDimension
        }
    }
    
    static func contentInset(_ value: UIEdgeInsets) -> Self {
        .wrap { item in
            item.contentInset = value
        }
    }
    
    static func keyboardDismissMode(_ value: UIScrollView.KeyboardDismissMode) -> Self {
        .wrap { item in
            item.keyboardDismissMode = value
        }
    }
    
    static func estimatedRowHeight(_ value: CGFloat) -> Self {
        .wrap { item in
            item.estimatedRowHeight = value
        }
    }
    
    static func isScrollEnabled(_ value: Bool) -> Self {
        .wrap { item in
            item.isScrollEnabled = value
        }
    }
    
    static func registerXibCells(_ values: [AnyClass]) -> Self {
        .wrap { item in
            item.registerXibCells(values)
        }
    }
    
    static func registerXibCell(_ value: AnyClass) -> Self {
        .wrap { item in
            item.registerXibCell(value)
        }
    }
    
    static func registerCells(_ value: [AnyClass]) -> Self {
        .wrap { item in
            value.forEach { item.registerCell($0) }
        }
    }
    
    static func registerCell(_ value: AnyClass) -> Self {
        .wrap { item in
            item.registerCell(value)
        }
    }
    
    static func registerHeaderFooterViewXib(_ value: AnyClass) -> Self {
        .wrap { item in
            item.registerHeaderFooterViewXib(value)
        }
    }
    
    static func registerHeaderFooterViewXibs(_ values: [AnyClass]) -> Self {
        .wrap { item in
            item.registerHeaderFooterViewXibs(values)
        }
    }
    
    static func registerHeaderFooterView(_ value: AnyClass) -> Self {
        .wrap { item in
            item.registerHeaderFooterView(value)
        }
    }
    
    static func delegate(_ value: UITableViewDelegate?) -> Self {
        .wrap { item in
            item.delegate = value
        }
    }
    
    static func dataSource(_ value: UITableViewDataSource?) -> Self {
        .wrap { item in
            item.dataSource = value
        }
    }
    
    static func tableFooterView(_ value: UIView?) -> Self {
        .wrap { item in
            item.tableFooterView = value
        }
    }
    
    static func tableHeaderView(_ value: UIView?) -> Self {
        .wrap { item in
            item.tableHeaderView = value
        }
    }
    
    static func refreshControl(_ value: UIRefreshControl?) -> Self {
        .wrap { item in
            item.refreshControl = value
        }
    }
    
    static func rowHeight(_ value: CGFloat) -> Self {
        .wrap { item in
            item.rowHeight = value
        }
    }
    
    static func separator(_ value: UITableView.SeparatorConfiguration?) -> Self {
        .wrap { item in
            if let value = value {
                item.separatorStyle = .singleLine
                item.separatorColor = value.color
                item.separatorInset = value.insets
                
            } else {
                item.separatorStyle = .none
                
            }
        }
    }
    
}
