
import UIKit

public extension StyleWrapper where Element: UIStackView {
    
    static func spacing(_ value: CGFloat) -> StyleWrapper {
        return .wrap { stackView in
            stackView.spacing = value
        }
    }
    
    static func distribution(_ value: UIStackView.Distribution) -> StyleWrapper {
        return .wrap { stackView in
            stackView.distribution = value
        }
    }
    
    static func axis(_ value: NSLayoutConstraint.Axis) -> StyleWrapper {
        return .wrap { stackView in
            stackView.axis = value
        }
    }
    
    static func vertical(_ spacing: CGFloat = 0) -> Self {
        .wrap { stackView in
            stackView.vertical(spacing)
        }
    }
    
    static func horizontal(_ spacing: CGFloat = 0) -> Self {
        .wrap { stackView in
            stackView.horizontal(spacing)
        }
    }
    
    static func alignment(_ value: UIStackView.Alignment) -> Self {
        .wrap { stackView in
            stackView.alignment = value
        }
    }
    
    static func removeArrangedSubviews() -> Self {
        .wrap { stackView in
            stackView.removeArrangedSubviews()
        }
    }
    
    static func setArrangedSubviews(_ value: [UIView]) -> Self {
        .wrap { stackView in
            stackView.removeArrangedSubviews()
            value.forEach { stackView.addArrangedSubview($0) }
        }
    }
    
    static func addArrangedSubview(_ value: UIView) -> Self {
        .wrap { stackView in
            stackView.addArrangedSubview(value)
        }
    }
    
    static func insets(_ value: UIEdgeInsets) -> StyleWrapper {
        .wrap { stackView in
            stackView.insets = value
        }
    }
    
}
