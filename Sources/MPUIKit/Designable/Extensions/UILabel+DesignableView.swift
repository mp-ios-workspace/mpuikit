
import UIKit

public extension StyleWrapper where Element: UILabel {
    
    static func numberOfLines(_ value: Int) -> Self {
        .wrap { label in
            label.numberOfLines = value
        }
    }
    
    static func textAlignment(_ value: NSTextAlignment) -> Self {
        .wrap { label in
            label.textAlignment = value
        }
    }
    
    static func text(_ value: String?) -> Self {
        .wrap { label in
            label.text = value
        }
    }
    
    static func attributedText(_ value: NSAttributedString?) -> Self {
        .wrap { label in
            label.attributedText = value
        }
    }
    
    static func textColor(_ color: UIColor?) -> Self {
        .wrap { label in
            label.textColor = color
        }
    }
    
    static func lineBreakMode(_ value: NSLineBreakMode) -> Self {
        .wrap { label in
            label.lineBreakMode = value
        }
    }
    
    static func font(_ value: UIFont) -> Self {
        .wrap { label in
            label.font = value
        }
    }
    
}
