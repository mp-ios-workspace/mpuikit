
import UIKit

public extension StyleWrapper where Element: UIActivityIndicatorView {
    
    static var `default`: Self {
        .wrap { activity in
            activity.hidesWhenStopped = true
            activity.style = .gray
        }
    }
    
    func color(_ value: UIColor) -> Self {
        .wrap { activity in
            activity.color = value
        }
    }
    
    func style(_ value: UIActivityIndicatorView.Style) -> Self {
        .wrap { activity in
            activity.style = value
        }
    }
    
    func hidesWhenStopped(_ value: Bool) -> Self {
        .wrap { activity in
            activity.hidesWhenStopped = value
        }
    }
    
}

public extension StyleWrapper where Element: UIRefreshControl {
    
    func tintColor(_ value: UIColor) -> Self {
        .wrap { item in
            item.tintColor = value
        }
    }
    
    func attributedTitle(_ value: NSAttributedString) -> Self {
        .wrap { item in
            item.attributedTitle = value
        }
    }
    
    @available(iOS 13.0, *)
    func largeContentTitle(_ value: String) -> Self {
        .wrap { item in
            item.largeContentTitle = value
        }
    }
    
}
