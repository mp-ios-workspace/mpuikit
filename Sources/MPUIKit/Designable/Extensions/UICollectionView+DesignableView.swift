//
//  File.swift
//  
//
//  Created by Alexander on 03.02.2022.
//

import UIKit

public extension StyleWrapper where Element: UICollectionView {
    
    static func `default`(_ scrollDirection: UICollectionView.ScrollDirection) -> Self {
        return .wrap { collection in
            collection.backgroundColor = .clear
            collection.showsVerticalScrollIndicator = false
            collection.showsHorizontalScrollIndicator = false
            let layout = collection.collectionViewLayout as? UICollectionViewFlowLayout
            layout?.scrollDirection = scrollDirection
            layout?.minimumLineSpacing = .zero
            layout?.minimumInteritemSpacing = .zero
        }
    }
    
    // MARK: - Property
    static func delegate(_ value: UICollectionViewDelegate?) -> Self {
        .wrap { item in
            item.delegate = value
        }
    }
    
    static func refreshControl(_ value: UIRefreshControl?) -> Self {
        .wrap { item in
            item.refreshControl = value
        }
    }
    
    static func dataSource(_ value: UICollectionViewDataSource?) -> Self {
        .wrap { item in
            item.dataSource = value
        }
    }
    
    static func isScrollEnabled(_ value: Bool) -> Self {
        .wrap { item in
            item.isScrollEnabled = value
        }
    }
    
    static func registerXibCells(_ values: [AnyClass]) -> Self {
        .wrap { item in
            item.registerXibCells(values)
        }
    }
    
    static func registerXibCell(_ value: AnyClass) -> Self {
        .wrap { item in
            item.registerXibCell(value)
        }
    }
    
    static func registerCells(_ value: [AnyClass]) -> Self {
        .wrap { item in
            value.forEach { item.registerCell($0) }
        }
    }
    
    static func registerCell(_ value: AnyClass) -> Self {
        .wrap { item in
            item.registerCell(value)
        }
    }
    
    static func registerSectionHeader(_ value: AnyClass) -> Self {
        .wrap { collectionView in
            collectionView.registerSectionHeader(value)
        }
    }
    
    static func registerSectionHeaders(_ value: [AnyClass]) -> Self {
        .wrap { collectionView in
            collectionView.registerSectionHeaders(value)
        }
    }
    
    static func registerSectionFooter(_ value: AnyClass) -> Self {
        .wrap { collectionView in
            collectionView.registerSectionFooter(value)
        }
    }
    
    static func registerSectionFooters(_ value: [AnyClass]) -> Self {
        .wrap { collectionView in
            collectionView.registerSectionFooters(value)
        }
    }
    
    static func keyboardDismissMode(_ value: UIScrollView.KeyboardDismissMode) -> StyleWrapper {
        .wrap { item in
            item.keyboardDismissMode = value
        }
    }
    
    static func minimumLineSpacing(_ value: CGFloat) -> Self {
        return .wrap { collection in
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing = value
        }
    }
    
    static func minimumInteritemSpacing(_ value: CGFloat) -> Self {
        return .wrap { collection in
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing = value
        }
    }
    
    static func spacing(_ value: CGFloat) -> Self {
        return .wrap { collection in
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing = value
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing = value
        }
    }
    
    static func contentInset(_ value: UIEdgeInsets) -> Self {
        return .wrap { collection in
            collection.contentInset = value
        }
    }
    
    static func itemSize(_ value: CGSize) -> Self {
        return .wrap { collection in
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize = value
        }
    }
    
    static func estimatedItemSize(_ value: CGSize) -> Self {
        return .wrap { collection in
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.estimatedItemSize = value
        }
    }
    
    static var itemSizeAutomaticSize: Self {
        return .wrap { collection in
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    
    static var estimatedItemSizeAutomaticSize: Self {
        return .wrap { collection in
            (collection.collectionViewLayout as? UICollectionViewFlowLayout)?.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    
}

