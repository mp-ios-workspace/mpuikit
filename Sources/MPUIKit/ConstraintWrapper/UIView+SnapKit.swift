
import UIKit
import SnapKit

public extension UIView {
    
    @discardableResult
    func addToSuperview(_ superview: UIView?, makeConstraints: MPDataClosure<ConstraintMaker>) -> Self {
        removeFromSuperview()
        
        guard let superview = superview else { return self }
        superview.addSubview(self)
        
        self.makeConstraints(makeConstraints)
        
        return self
    }
    
    @discardableResult
    func insertToSuperview(_ superview: UIView?, belowSubview siblingSubview: UIView?, makeConstraints: MPDataClosure<ConstraintMaker>) -> Self {
        removeFromSuperview()

        guard let superview = superview, let siblingSubview = siblingSubview else { return self }
        superview.insertSubview(self, belowSubview: siblingSubview)
        
        self.makeConstraints(makeConstraints)
        
        return self
    }
    
    @discardableResult
    func makeConstraints(_ makeConstraints: MPDataClosure<ConstraintMaker>) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        snp.makeConstraints(makeConstraints)
        
        return self
    }
    
}
