
import UIKit

public extension UIView {
    
    @discardableResult
    func addToSuperview(_ superview: UIView?, withConstraints constraints: [ConstraintWrapper]) -> Self {
        removeFromSuperview()
        
        guard let superview = superview else { return self }
        translatesAutoresizingMaskIntoConstraints = false
        superview.addSubview(self)
        
        return setupConstraints(constraints)
    }
    
    @discardableResult
    func insertToSuperview(_ superview: UIView?, belowSubview siblingSubview: UIView?, withConstraints constraints: [ConstraintWrapper]) -> Self {
        removeFromSuperview()

        guard let superview = superview, let siblingSubview = siblingSubview else { return self }
        translatesAutoresizingMaskIntoConstraints = false
        superview.insertSubview(self, belowSubview: siblingSubview)
        
        return setupConstraints(constraints)
    }
    
    @discardableResult
    func setupConstraints(_ constraints: [ConstraintWrapper]) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        constraints.forEach({
            switch $0 {
            case let .wrap(constraint):
                constraint(self)
            }
        })
        
        return self
    }
    
}
