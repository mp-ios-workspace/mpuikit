
import UIKit

public extension UICollectionView {
    
    func registerXibCell(_ value: AnyClass) {
        let description = String(describing: value.self)
        let bundle = Bundle(for: value.self)
        let nib = UINib(nibName: description, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: description)
    }
    
    func registerXibCells(_ values: [AnyClass]) {
        values.forEach { registerXibCell($0) }
    }
    
    func registerCell(_ value: AnyClass) {
        register(value, forCellWithReuseIdentifier: .init(describing: value.self))
    }
    
    func registerCells(_ value: [AnyClass]) {
        value.forEach { registerCell($0) }
    }
    
    func registerSectionHeader(_ value: AnyClass) {
        register(
            value,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: String(describing: value.self)
        )
    }
    
    func registerSectionHeaders(_ value: [AnyClass]) {
        value.forEach { registerSectionHeader($0) }
    }
    
    func registerSectionFooter(_ value: AnyClass) {
        register(
            value,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: String(describing: value.self)
        )
    }
    
    func registerSectionFooters(_ value: [AnyClass]) {
        value.forEach { registerSectionFooter($0) }
    }
    
}
