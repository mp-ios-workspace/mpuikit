
import UIKit

public extension UIBezierPath {

    convenience init(roundedRectFromCenter frame: CGRect, cornerRadius: CGFloat) {
        self.init()

        move(to: CGPoint(x: frame.width / 2, y: 0))
        addLine(to: CGPoint(x: frame.width - cornerRadius, y: 0))
        addArc(
            withCenter: CGPoint(x: frame.width - cornerRadius, y: cornerRadius),
            radius: cornerRadius,
            startAngle: -.pi / 2,
            endAngle: 0,
            clockwise: true
        )
        addLine(to: CGPoint(x: frame.width, y: frame.height - cornerRadius))
        addArc(
            withCenter: CGPoint(x: frame.width - cornerRadius, y: frame.height - cornerRadius),
            radius: cornerRadius,
            startAngle: 0,
            endAngle: .pi / 2,
            clockwise: true
        )
        addLine(to: CGPoint(x: cornerRadius, y: frame.height))
        addArc(
            withCenter: CGPoint(x: cornerRadius, y: frame.height - cornerRadius),
            radius: cornerRadius,
            startAngle: .pi / 2,
            endAngle: .pi,
            clockwise: true
        )
        addLine(to: CGPoint(x: 0, y: cornerRadius))
        addArc(
            withCenter: CGPoint(x: cornerRadius, y: cornerRadius),
            radius: cornerRadius,
            startAngle: .pi,
            endAngle: .pi * 3 / 2,
            clockwise: true
        )

        close()
        apply(CGAffineTransform(
            translationX: frame.origin.x,
            y: frame.origin.y
        ))
    }

}
