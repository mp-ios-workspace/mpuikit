
public typealias MPClosure = () -> Void
public typealias MPDataClosure<T> = (T) -> Void
