
import UIKit

public extension UIView {
    
    enum Direction {
        case topToBottom
        case bottomToTop
        case leftToRight
        case rightToLeft
        case topLeftToBottomRight
        case topRightToBottomLeft
        case bottomLeftToTopRight
        case bottomRightToTopLeft
        
        var value: (CGPoint, CGPoint) {
            switch self {
            case .topToBottom:
                return (CGPoint(x: 0.5, y: 0), CGPoint(x: 0.5, y: 1))
            case .bottomToTop:
                return (CGPoint(x: 0.5, y: 1), CGPoint(x: 0.5, y: 0))
            case .leftToRight:
                return (CGPoint(x: 0, y: 0.5), CGPoint(x: 1, y: 0.5))
            case .rightToLeft:
                return (CGPoint(x: 1, y: 0.5), CGPoint(x: 0, y: 0.5))
            case .topLeftToBottomRight:
                return (CGPoint(x: 0, y: 0), CGPoint(x: 1, y: 1))
            case .topRightToBottomLeft:
                return (CGPoint(x: 1, y: 1), CGPoint(x: 0, y: 0))
            case .bottomLeftToTopRight:
                return (CGPoint(x: 0, y: 1), CGPoint(x: 1, y: 0))
            case .bottomRightToTopLeft:
                return (CGPoint(x: 1, y: 1), CGPoint(x: 0, y: 0))
            }
        }
    }
    
    var verticalCompression: Float {
        get { contentCompressionResistancePriority(for: .vertical).rawValue }
        set { setContentCompressionResistancePriority(.init(newValue), for: .vertical) }
    }
    
    var horizontalCompression: Float {
        get { contentCompressionResistancePriority(for: .horizontal).rawValue }
        set { setContentCompressionResistancePriority(.init(newValue), for: .horizontal) }
    }
    
    var verticalHugging: Float {
        get { contentHuggingPriority(for: .vertical).rawValue }
        set { setContentHuggingPriority(.init(newValue), for: .vertical) }
    }
    
    var horizontalHugging: Float {
        get { contentHuggingPriority(for: .horizontal).rawValue }
        set { setContentHuggingPriority(.init(newValue), for: .horizontal) }
    }
    
    var createImage: UIImage {
        let renderer = UIGraphicsImageRenderer(size: bounds.size)
        return renderer.image { ctx in
            drawHierarchy(in: bounds, afterScreenUpdates: true)
        }
    }
    
    var allSubviews: [UIView] {
        return subviews.flatMap { [$0] + $0.allSubviews }
    }
    
    var isVisible: Bool {
        get { !isHidden }
        set { isHidden = !newValue }
    }
    
    var cornerRadius: CGFloat {
        get { layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
    
    var borderColor: UIColor? {
        get {
            guard let cgColor = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: cgColor)
        }
        set { layer.borderColor = newValue?.cgColor }
    }
    
    var borderWidth: CGFloat {
        get { layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    var automaticDimension: CGSize {
        systemLayoutSizeFitting(.zero)
    }
    
    func addToSuperview(_ value: UIView) {
        removeFromSuperview()
        value.addSubview(self)
    }
    
    func addSubviews(_ values: [UIView]) {
        values.forEach { addSubview($0) }
    }
    
    func addSublayer(_ sublayer: CALayer) {
        layer.addSublayer(sublayer)
    }
    
    func removeConstraint(_ constraint: NSLayoutConstraint?) {
        guard let constraint = constraint else {
            return
        }
        
        removeConstraint(constraint)
    }
    
    func setGradient(direction: Direction, colors: [UIColor?], locations: [NSNumber]? = nil) {
        let gradientBackgroundKey = "UIView.gradientBackgroundKey"
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors.compactMap({ $0?.cgColor })
        gradientLayer.startPoint = direction.value.0
        gradientLayer.endPoint = direction.value.1
        gradientLayer.locations = locations
        gradientLayer.frame = bounds
        gradientLayer.name = gradientBackgroundKey
        
        layer.masksToBounds = true
        layer.sublayers?.removeAll(where: { $0.name == gradientBackgroundKey })
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setShadow(color: UIColor?, offset: CGSize, radius: CGFloat = 3, opacity: Float = 0.1) {
        layer.shadowColor = color?.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
    }
    
    func nextBecomeFirstResponder() {
        for view in superview?.allSubviews ?? [] {
            guard view !== self, view.canBecomeFirstResponder else {
                endEditing(true)
                continue
            }
            
            view.becomeFirstResponder()
            break
        }
    }
    
    func calculatePreferredHeight(preferredWidth: CGFloat) -> CGFloat {
        let widthConstraint = NSLayoutConstraint(
            item: self,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1,
            constant: preferredWidth
        )
        addConstraint(widthConstraint)
        let height = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        removeConstraint(widthConstraint)
        return height
    }
    
    //MARK: - Animation
    
    func removeFromSuperviewAnimated(with duration: TimeInterval) {
        UIView.animate(withDuration: duration) { [weak self] in
            self?.alpha = 0
        } completion: { [weak self] _ in
            self?.removeFromSuperview()
        }
    }
    
    func transitionCrossDissolve(duration: TimeInterval) {
        UIView.transition(
            with: self,
            duration: duration,
            options: .transitionCrossDissolve,
            animations: nil
        )
    }
    
    func layoutAnimated(duration: TimeInterval) {
        UIView.animate(withDuration: duration) {
            self.layoutIfNeeded()
        }
    }
    
    static func shake(
        view: UIView,
        for duration: TimeInterval = 0.5,
        withTranslation translation: CGFloat = 15,
        completion: MPClosure? = nil
    ) {
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.3) {
            view.transform = CGAffineTransform(translationX: translation, y: 0)
        }

        propertyAnimator.addAnimations({
            view.transform = CGAffineTransform(translationX: 0, y: 0)
        }, delayFactor: 0.2)
        
        propertyAnimator.addCompletion { _ in
            completion?()
        }

        propertyAnimator.startAnimation()
    }
    
}
