//
//  File.swift
//  
//
//  Created by Alexander on 03.02.2022.
//

import UIKit

public extension UIViewController {
    
    var safeAreaGuide: UILayoutGuide {
        view.safeAreaLayoutGuide
    }
    
    var isModal: Bool {
        let presentingIsModal = presentingViewController != nil
        var presentingIsNavigation: Bool {
            guard let navigation = navigationController else {
                return false
            }
            return navigation.presentingViewController?.presentedViewController == navigation
        }
            
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
        
        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
    
    func addAsChildToParent(_ parent: UIViewController) {
        parent.addChild(self)
        didMove(toParent: parent)
        willMove(toParent: parent)
    }
    
    func removeAsChildFromParent() {
        removeFromParent()
        willMove(toParent: nil)
        didMove(toParent: nil)
    }
    
}
