
import UIKit

public extension UITableViewCell {
    
    static var reuseID: String {
        .init(describing: Self.self)
    }
    
    var tableView: UITableView? {
        superview as? UITableView
    }
    
    var indexPath: IndexPath? {
        tableView?.indexPath(for: self)
    }
    
}
