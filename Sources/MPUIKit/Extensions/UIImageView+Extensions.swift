
import UIKit
import SDWebImage

public extension UIImageView {
    
    struct MPImage {
        public let url: String?
        public var placeholder: UIImage?
        public var progress: ((_ progress: Int, _ expected: Int) -> Void)?
        public var didLoad: MPDataClosure<UIImage?>?
        public var indicator: SDWebImageIndicator?
        public var transition: SDWebImageTransition?
        
        public init(url: String?, placeholder: UIImage? = nil, progress: ((Int, Int) -> Void)? = nil, didLoad: MPDataClosure<UIImage?>? = nil, indicator: SDWebImageIndicator? = nil, transition: SDWebImageTransition? = nil) {
            self.url = url
            self.placeholder = placeholder
            self.progress = progress
            self.didLoad = didLoad
            self.indicator = indicator
            self.transition = transition
        }
    }
    
    func load(image: MPImage) {
        guard let stringUrl = image.url, let url = URL(string: stringUrl) else {
            return
        }
        
        self.image = image.placeholder
        sd_imageIndicator = image.indicator
        sd_imageTransition = image.transition
        
        sd_setImage(
            with: url,
            placeholderImage: image.placeholder,
            options: []
        ) { progress, expected, _ in
            image.progress?(progress, expected)
        } completed: { loadedImage, _, _, _ in
            image.didLoad?(loadedImage)
        }
    }
    
}
