
import UIKit

public extension UITableView {
    
    struct SeparatorConfiguration {
        var color: UIColor? = .systemGray
        var insets: UIEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 16)
        
        public init(color: UIColor? = .systemGray, insets: UIEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 16)) {
            self.color = color
            self.insets = insets
        }
    }
    
    func registerXibCell(_ value: AnyClass) {
        let description = String(describing: value.self)
        let bundle = Bundle(for: value.self)
        let nib = UINib(nibName: description, bundle: bundle)
        register(nib, forCellReuseIdentifier: description)
    }
    
    func registerXibCells(_ values: [AnyClass]) {
        values.forEach { registerXibCell($0) }
    }
    
    func registerCell(_ value: AnyClass) {
        register(value, forCellReuseIdentifier: .init(describing: value.self))
    }
    
    func registerHeaderFooterView(_ value: AnyClass) {
        register(value, forHeaderFooterViewReuseIdentifier: .init(describing: Self.self))
    }
    
    func registerHeaderFooterViews(_ values: [AnyClass]) {
        values.forEach { registerHeaderFooterView($0) }
    }
    
    func registerHeaderFooterViewXib(_ value: AnyClass) {
        let description = String(describing: value.self)
        let bundle = Bundle(for: value.self)
        let nib = UINib(nibName: description, bundle: bundle)
        
        register(nib, forHeaderFooterViewReuseIdentifier: description)
    }

    func registerHeaderFooterViewXibs(_ values: [AnyClass]) {
        values.forEach { registerHeaderFooterViewXib($0) }
    }
    
    func calculateBottomOffset() -> CGPoint {
        let y =
            self.contentSize.height -
            self.frame.size.height +
            self.contentInset.top +
            self.contentInset.bottom

        return .init(x: 0, y: y)
    }
    
}
