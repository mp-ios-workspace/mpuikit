
import UIKit

public extension UIStackView {
    
    var insets: UIEdgeInsets {
        get { layoutMargins }
        set {
            isLayoutMarginsRelativeArrangement = true
            layoutMargins = newValue
        }
    }
    
    func vertical(_ spacing: CGFloat = 0) {
        self.spacing = spacing
        axis = .vertical
        distribution = .fill
        alignment = .fill
    }
    
    func horizontal(_ spacing: CGFloat = 0) {
        self.spacing = spacing
        axis = .horizontal
        distribution = .fill
        alignment = .fill
    }
    
    func removeArrangedSubviews() {
        arrangedSubviews.forEach {
            removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
    
    func setArrangedSubviews(_ subviews: [UIView]) {
        removeArrangedSubviews()
        subviews.forEach { addArrangedSubview($0) }
    }
    
    func removeArrangedSubview(_ view: UIView?) {
        guard let view = view else {
            return
        }
        
        removeArrangedSubview(view)
        view.removeFromSuperview()
    }
    
}
