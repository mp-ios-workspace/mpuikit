
import UIKit

extension NSAttributedString {
    
    public struct Underline {
        let color: UIColor?
        let style: NSUnderlineStyle
        
        public init(color: UIColor?, style: NSUnderlineStyle) {
            self.color = color
            self.style = style
        }
    }
    
    public struct Stroke {
        let color: UIColor?
        let width: CGFloat
        
        public init(color: UIColor?, width: CGFloat) {
            self.color = color
            self.width = width
        }
    }
    
    public struct Shadow {
        let color: UIColor?
        let radius: CGFloat
        let offset: CGSize
        
        public init(color: UIColor?, radius: CGFloat, offset: CGSize) {
            self.color = color
            self.radius = radius
            self.offset = offset
        }
    }
    
    public enum Attribute {
        case font(UIFont)
        case paragraph(NSMutableParagraphStyle)
        case backgroundColor(UIColor?)
        case textColor(UIColor?)
        case underline(Underline)
        case kern(CGFloat)
        case strikethrough(Underline)
        case stroke(Stroke)
        case shadow(Shadow)
        case baselineOffset(Float)
        case link(String)
        
        public var value: [Key : Any] {
            switch self {
            case let .font(value):
                return [.font : value]
                
            case let .paragraph(value):
                return [.paragraphStyle : value]
                
            case let .textColor(value):
                guard let color = value else {
                    return [:]
                }
                return [.foregroundColor : color]
                
            case let .underline(value):
                guard let color = value.color else {
                    return [.underlineStyle : value.style]
                }
                
                return [.underlineStyle : value.style.rawValue, .underlineColor : color]
                
            case let .kern(value):
                return [.kern : value]
            case let .strikethrough(value):
                guard let color = value.color else {
                    return [.strikethroughStyle : value.style]
                }
                
                return [.strikethroughStyle : value.style, .strikethroughColor : color]
                
            case let .stroke(value):
                guard let color = value.color else {
                    return [.strokeWidth : value.width]
                }
                
                return [.strokeWidth : value.width, .strokeColor : color]
                
            case let .shadow(value):
                let shadow = NSShadow()
                shadow.shadowColor = value.color
                shadow.shadowBlurRadius = value.radius
                shadow.shadowOffset = value.offset
                
                return [.shadow : shadow]
                
            case let .backgroundColor(value):
                guard let color = value else {
                    return [:]
                }
                return [.backgroundColor : color]
                
            case let .baselineOffset(value):
                return [.baselineOffset : NSNumber(value: value)]

            case let .link(urlString):
                guard let url = URL(string: urlString) else {
                    return [:]
                }
                
                return [.link : url]
                
            }
        }
    }
    
}

public extension NSMutableAttributedString {
    
    fileprivate var range: NSRange {
        return NSRange(location: 0, length: length)
    }
    
    fileprivate var paragraphStyle: NSMutableParagraphStyle? {
        return attributes(at: 0, effectiveRange: nil)[.paragraphStyle] as? NSMutableParagraphStyle
    }
    
    fileprivate func addAttributes(_ value: [Attribute]) {
        let attributes = value.reduce([:], {
            $0.merging($1.value, uniquingKeysWith: { (_, new) in new })
        })
        addAttributes(attributes, range: range)
    }
    
    convenience init(text: String, attributes: [Attribute]) {
        self.init(string: text)
        addAttributes(attributes)
    }
    
    static func +(lhs: NSMutableAttributedString, rhs: NSMutableAttributedString) -> NSMutableAttributedString {
        let lhs = NSMutableAttributedString(attributedString: lhs)
        lhs.append(rhs)
        
        return lhs
    }

    static func +=(lhs: NSMutableAttributedString, rhs: NSMutableAttributedString) {
        lhs.append(rhs)
    }
    
    static func +=(lhs: NSMutableAttributedString, image: UIImage?) {
        guard let image = image else {
            return
        }
        
        let font = lhs.attributes(at: 0, effectiveRange: nil)[.font] as? UIFont
        let capHeight = font?.capHeight ?? 0
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(
            x: 0,
            y: (capHeight - image.size.height).rounded() / 2,
            width: image.size.width,
            height: image.size.height
        )
        
        let spacing = NSAttributedString(string: " ")
        let rhs = NSAttributedString(attachment: attachment)
        lhs.append(spacing)
        lhs.append(rhs)
    }

    
}
