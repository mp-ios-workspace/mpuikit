//
//  File.swift
//  
//
//  Created by Alexander on 06.04.2022.
//

import Foundation

public extension IndexPath {
    
    static var zero: IndexPath {
        .init(row: 0, section: 0)
    }
    
}
