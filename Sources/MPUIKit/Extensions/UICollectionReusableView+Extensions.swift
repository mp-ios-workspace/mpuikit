
import UIKit

public extension UICollectionReusableView {
    
    static var reuseID: String {
        .init(describing: Self.self)
    }
    
}
