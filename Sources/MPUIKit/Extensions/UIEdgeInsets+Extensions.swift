
import Foundation
import UIKit

public extension UIEdgeInsets {
    
    init(inset: CGFloat) {
        self.init(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    init(topBottom: CGFloat) {
        self.init(top: topBottom, left: 0, bottom: topBottom, right: 0)
    }
    
    init(leftRight: CGFloat) {
        self.init(top: 0, left: leftRight, bottom: 0, right: leftRight)
    }
    
    init(top: CGFloat) {
        self.init(top: top, left: 0, bottom: 0, right: 0)
    }
    
    init(left: CGFloat) {
        self.init(top: 0, left: left, bottom: 0, right: 0)
    }
    
    init(bottom: CGFloat) {
        self.init(top: 0, left: 0, bottom: bottom, right: 0)
    }
    
    init(right: CGFloat) {
        self.init(top: 0, left: 0, bottom: 0, right: right)
    }
    
}
