# MPUIKit

## Как установить

1. Xcode
2. File Drop Down menu
3. Swift Packages
4. Add Package Dependency
5. Copy and paste url **https://bitbucket.org/mp-ios-workspace/mpuikit**

---
## Как использовать

Библиотека создана для создания стилей и декларативного обращения к свойствам класса **UIView** и его наследников, что поможет упростить/ускорить процесс создания пользовательского интерфейса.

---
## Template

1. Template для создания архитектурных файлов можно скачать **https://bitbucket.org/mp-ios-workspace/mptemplates/src/master/**
